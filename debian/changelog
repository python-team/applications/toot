toot (0.47.1-2) unstable; urgency=medium

  * Use GitHub releases instead of PyPI for uscan 

 -- Jonathan Carter <jcc@debian.org>  Mon, 06 Jan 2025 12:30:52 +0200

toot (0.47.1-1) unstable; urgency=medium

  * New upstream release 

 -- Jonathan Carter <jcc@debian.org>  Sat, 30 Nov 2024 18:52:08 +0200

toot (0.45.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 07 Oct 2024 12:46:10 +0200

toot (0.44.1-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.7.0
  * Add build-dependency: pybuild-plugin-pyproject
  * Add python3-pil to recommends (needed to display blocky images in TUI)
  * Remove trailing whitespace found elsewhere in this changelog

 -- Jonathan Carter <jcc@debian.org>  Thu, 15 Aug 2024 18:01:08 +0200

toot (0.42.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sun, 24 Mar 2024 17:46:34 +0200

toot (0.41.1-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 05 Feb 2024 13:16:54 +0200

toot (0.39.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sun, 26 Nov 2023 20:32:24 +0200

toot (0.38.1-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sun, 13 Aug 2023 18:40:02 +0200

toot (0.36.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 19 Jun 2023 16:58:16 +0200

toot (0.35.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Tue, 07 Mar 2023 10:35:24 +0200

toot (0.34.1-1) unstable; urgency=medium

  * New upstream release (Closes: #1030018)
  * Update copyright years
  * Update standards version to 4.6.2

 -- Jonathan Carter <jcc@debian.org>  Fri, 10 Feb 2023 10:56:12 +0200

toot (0.32.1-2) unstable; urgency=medium

  * Upload fix to typos in man page

 -- Jonathan Carter <jcc@debian.org>  Fri, 23 Dec 2022 17:33:39 +0200

toot (0.32.1-1) unstable; urgency=medium

  * New upstream release (Closes: #1026422)

 -- Jonathan Carter <jcc@debian.org>  Fri, 23 Dec 2022 17:23:09 +0200

toot (0.30.1-1) unstable; urgency=medium

  * New upstream release (Closes: #1025091)

 -- Jonathan Carter <jcc@debian.org>  Thu, 01 Dec 2022 10:37:57 +0200

toot (0.30.0-1) unstable; urgency=medium

  * New upstream release
  * Merge changes back from git and fix debian/changelog

 -- Jonathan Carter <jcc@debian.org>  Thu, 01 Dec 2022 09:12:21 +0200

toot (0.29.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Tue, 29 Nov 2022 10:08:50 +0200

toot (0.28.1-2) unstable; urgency=medium

  * Re-introduce previous janitor changes
    (upstream metadata, clear whitespace)

 -- Jonathan Carter <jcc@debian.org>  Sat, 19 Nov 2022 12:02:58 +0200

toot (0.28.1-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sat, 19 Nov 2022 11:51:32 +0200

toot (0.28.0-2) unstable; urgency=medium

  [ Jonathan Carter ]
  * Merge changes from git

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Sep 2021 09:18:43 +0200

toot (0.28.0-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.6.1
  * Upgrade to debhelper-compat level 13
  * Update copyright years
  * Update debian/watch version to 4

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Sep 2021 09:08:53 +0200

toot (0.27.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Tue, 30 Jun 2020 18:27:27 +0200

toot (0.26.0-1) unstable; urgency=medium

  * New upstream release
  * Declare Rules-Requires-Root: no
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Sat, 18 Apr 2020 16:13:43 +0200

toot (0.25.2-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.5.0

 -- Jonathan Carter <jcc@debian.org>  Wed, 29 Jan 2020 13:19:09 +0200

toot (0.24.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 23 Sep 2019 07:54:30 +0000

toot (0.23.1-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 09 Sep 2019 12:52:20 +0000

toot (0.22.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Jonathan Carter ]
  * New upstream release
  * Remove Makefile rm from debian/rules (fixed upstream)

 -- Jonathan Carter <jcc@debian.org>  Sun, 04 Aug 2019 14:41:55 +0200

toot (0.21.0-1) unstable; urgency=medium

  * New upstream release
  * Upgrade to debhelper-compat (=12)
  * Add new build-dependency: python3-wcwidth
  * Add dh_install_override to remove extraneous Makefile
  * Remove unneeded shlib depends

 -- Jonathan Carter <jcc@debian.org>  Mon, 18 Feb 2019 10:29:24 +0000

toot (0.20.0-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.3.0

 -- Jonathan Carter <jcc@debian.org>  Fri, 04 Jan 2019 15:48:36 +0200

toot (0.19.0-1) unstable; urgency=medium

  [ Jonathan Carter ]
  * New upstream release
  * Update standards version to 4.1.5
  * Update copyright years

  [ Ondřej Nový ]
  * d/control: Add Vcs-* field

 -- Jonathan Carter <jcc@debian.org>  Sat, 21 Jul 2018 14:01:20 +0200

toot (0.18.0-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Fri, 15 Jun 2018 14:02:52 +0200

toot (0.17.1-1) unstable; urgency=medium

  * Initial release (Closes: #898546)

 -- Jonathan Carter <jcc@debian.org>  Sun, 13 May 2018 11:40:49 +0200
